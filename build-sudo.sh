#!/bin/zsh
#
# Make an ISO set for a given distribution

set -x

# to avoid perl complains about not configured locales
export LC_ALL=C

SUITE=${1:-daedalus}
ISOSET=${2:-all}
DISTVERSION=${3:-5.0}
: ${SOURCE:=http://pkgmaster.devuan.org/merged}
: ${ARCH:=$(dpkg-architecture -q DEB_HOST_ARCH)}
: ${TARGET:=$SUITE.${ARCH}.fs}

if [ $(id -u) = 0 ] ; then
    alias sudo=""
fi

if [ -n "${http_proxy}" ] ; then
    echo "# Using http_proxy ${http_proxy}" >&2
    PROXY="${http_proxy}"
fi

# helper
function intarget() {
    echo sudo chroot $TARGET "$@" >&2
    sudo chroot $TARGET "$@"
}

if [ -e $TARGET ]; then
    echo "# Clean up prior building..."
    findmnt $TARGET/dev/pts && sudo umount -v $TARGET/dev/pts
    sudo rm -rf $TARGET
    echo
fi

echo "# Set up a chroot environment to build in" >&2
if [ ! -e /usr/share/debootstrap/scripts/$SUITE ] ; then
    echo "MISSING debootstrap script /usr/share/debootstrap/scripts/$SUITE" >&2
    exit 1
fi

sudo env http_proxy=${http_proxy} debootstrap \
     --arch ${ARCH} --variant=minbase --no-merged-usr \
     $SUITE $TARGET $SOURCE
echo

echo "# Configure apt" >&1
cat <<EOF | sudo tee $TARGET/etc/apt/sources.list > /dev/null
deb $SOURCE $SUITE main contrib non-free
deb-src $SOURCE $SUITE main contrib non-free
deb $SOURCE $SUITE main/debian-installer
EOF

cat <<EOF | sudo tee $TARGET/etc/apt/apt.conf.d/02local > /dev/null
$([ -z "${http_proxy}" ] || echo "Acquire::http::proxy \"${http_proxy}\";")
Binary::apt::APT::Keep-Downloaded-Packages "true";
APT::Install-Recommends "0";
APT::Sandbox::User "root";
EOF

intarget apt-get update

# next apt-get command will try to write to /dev/pts
echo "# Set up /dev/pts"
intarget mount -t devpts none /dev/pts

ARCH="$(dpkg-architecture -q DEB_HOST_ARCH)"
echo
echo "# Install required build host packages except grub" >&2
case "$ARCH" in
    amd64)
	BUILDPKGS=(
	    linux-image-amd64
	    build-essential newlisp isolinux syslinux-common syslinux-utils
	    fakeroot libbogl-dev bf-utf-source grub-efi-amd64-bin # grub-pc
	    dosfstools mtools win32-loader tofrodos xorriso
	    zsh debhelper po-debconf wget zstd
	    grub-efi-ia32-bin
	)
	;;
    i386)
	BUILDPKGS=(
	    linux-image-686
	    build-essential newlisp isolinux syslinux-common syslinux-utils
	    fakeroot libbogl-dev bf-utf-source
	    dosfstools mtools win32-loader tofrodos xorriso
	    zsh debhelper po-debconf wget zstd
	    grub-efi-ia32-bin
	)
	;;
    *)
	echo "Unknown architecture: $ARCH" >&2
	exit 1
	;;
esac
intarget apt-get -y install $BUILDPKGS 

echo "# Transfer the installer-iso package" >&2
DATA=(
    ././Makefile ././*.mk ././udeb-sets.mk.tmpl ././scripts ././runbuild.sh
    ././boot ././docs ././needed-characters ././pool ././localudebs
)
sudo mkdir $TARGET/installer-iso
sudo rsync -avR $DATA $TARGET/installer-iso/

echo "# Build all" >&2
ENV="SUITE=${SUITE} DISTVERSION=${DISTVERSION}"
intarget /bin/bash -c "cd installer-iso && env $ENV ./runbuild.sh $ISOSET"
if [ -z "TRIAL" ] ; then
    rsync -av $TARGET/installer-iso/content .
fi
sudo umount -v $TARGET/dev/pts
