#!/bin/bash
#
# This script is given one or more udeb sets as arguments. It firstly
# strips any line with a # in it from any set, and then determines the
# applicable package dependency closure from remaining packages.
#
# The udeb sets are typically defined in multi-line define statements
# in makefiles with commenting using "#".
#
# This script is run in the build root directory with the "pool"
# subdirectory.

echo $0 with ${#} udeb collections >&2

set -e

ARCH=$(dpkg-architecture -qDEB_HOST_ARCH)

# Build the package libraries if needed
if [ ! -d pool/library-$ARCH-main_debian-installer ] ; then
    # Trigger a library building using NETINSTALL as example
    make -C pool default clean ISO=NETINSTALL 1>&2 || exit 1
fi

# Expand udebs to include all dependent and recommended recursively
for x in "$@" ; do echo "$x" ; done > udebs-initial
( cd pool && ./review.lsp ../udebs-initial ) > udebs-implied
grep -v '#' udebs-implied
