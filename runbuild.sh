#!/bin/bash
#
# Require env SUITE and DISTVERSION

set -x

cd $(readlink -f $(dirname $0))
PATH=$PATH:$(pwd)/scripts
echo "runbuild: $PATH" >&2
export LOCALUDEBS=$(pwd)/localudebs/$(arch)

if [ "${1-all}" = all ] ; then
    make all DISTNAME=${SUITE} DISTVERSION=${DISTVERSION}
else
    make build ISO=$1 DISTNAME=${SUITE} DISTVERSION=${DISTVERSION}
fi
