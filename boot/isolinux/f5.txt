71F507                  0fWelcome to Devuan GNU/Linux!07

    Devuan ${DEBIAN_VERSION} installer built on ${BUILD_DATE}.






    0fSPECIAL BOOT PARAMETERS - OVERVIEW07

On a few systems, you may need to specify a parameter at the boot prompt in
order to boot the system. For example, Linux may not be able to autodetect
your hardware, and you may need to explicitly specify its location or type
for it to be recognized.

For more information about what boot parameters you can use, press:

 <0fF607> -- boot parameters for special machines
 <0fF707> -- boot parameters for various disk controllers
 <0fF807> -- boot parameters understood by the install system

Note that to specify a parameter for a particular kernel module, use the
form module.param=value, for example: libata.atapi_enabled=1



Press F1 through F10 for more help or ENTER to return to the menu.
