# Makefile fragments for preparing different package collections
# into the ${ISOTREE}

COLLECTIONS = NETINSTALL INSTALL DVD1 CD2 CD3 CD4 POOL1 POOL2

${warning ISO = ${ISO} (of ${COLLECTIONS})}

define INSTALLPACKAGES
.PHONY: pool.$1
pool.$1:
	$(MAKE) -C pool ISO=$${@:pool.%=%} CODENAME=${DISTNAME}
	gen-pool ${ISOTREE} ${DISTNAME} ${DISTVERSION} $${@:pool.%=%}
endef

$(foreach C,${COLLECTIONS},$(eval ${call INSTALLPACKAGES,$C}))
