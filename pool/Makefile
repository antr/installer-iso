# Rules to build the local Packages files for a ${ISO}

#CODENAME := ???
SOURCEHOST := pkgmaster.devuan.org
ARCH := $(shell dpkg-architecture -qDEB_HOST_ARCH)
HEAD := ${ISO}_dists_${CODENAME}
TAIL := binary-${ARCH}_Packages
BKV := ${shell ls -r /lib/modules | head -n 1 | sed s/-pae//}

by_vote-00: LIMIT = 2500

SECTIONS = main contrib non-free main_debian-installer

INSTALLER = firmware installer-menu installer-undeclared minbase extra
ifeq (${CODENAME},beowulf)
INSTALLER += extra-beowulf
endif

BY_PRIORITY = all-required all-important all-standard

KERNELS = amd64/linux-image-amd64 i386/linux-image-686
KERNELS += ${ARCH}/linux-image-${BKV}

ifeq (${ARCH},i386)
KERNELS += i386/linux-image-${BKV}-pae
endif


KERNEL := ${notdir ${filter ${ARCH}/%,${KERNELS}}}
${warning KERNEL for ${ARCH} is ${KERNEL}}

default: ${SECTIONS:%=${HEAD}_%_${TAIL}}

# Set up a library-${ARCH}-* directory for a section
APTPFX := /var/lib/apt/lists/${SOURCEHOST}_merged_dists_${CODENAME}
APTPFX2 := /var/lib/apt/lists/${SOURCEHOST}_devuan_dists_${CODENAME}

# Dummy rule for nonexistent Packages files
%_Packages:
	:

# Generic build rule for each section
define makelibrary
library-${ARCH}-$1: ${APTPFX}_$1_${TAIL} \
		    ${APTPFX}-security_$1_${TAIL} \
		    ${APTPFX2}-updates_$1_${TAIL} \
		    ${APTPFX2}-proposed-updates_$1_${TAIL}
	-for f in $$^ ; do [ -e "$$$$f" ] && ./library.lsp $$@ $$$$f ; done
	./drop_wrong_kernel.lsp '$$@' '${KERNEL}'
endef
$(foreach S,$(SECTIONS),$(eval $(call makelibrary,$S)))

${BY_PRIORITY}: ${SECTIONS:%=library-${ARCH}-%}
	grep "Priority: ${@:all-%=%}" \
	    -r library-${ARCH}-* | \
	    sed 's/.*\///' | sed 's/:.*//' | sort -u > $@

SKIPLIST ?= /dev/null

# Prepare expanded package with early exclusions
define prepare_list
	grep -hv '#' $^ | sort -u | comm -23 - ${SKIPLIST} > A
	yes | ./review.lsp -r A -- ${KERNEL} > $@
	rm -f A
	if grep UNAVAILABLE $@ >&2 ; then exit 1 ; else true ; fi
endef

# Prepare expanded package with late exclusions
HIDE=../scripts/hidesome.sh
define prepare_pool_list
	yes | ./review.lsp -r $^ -- ${KERNEL} | ${HIDE} ${SKIPLIST} > $@
	if grep UNAVAILABLE $@ ; then exit 1 ; else true ; fi
endef

# include linux-headers-<version> for all included kernels
.INTERMEDIATE: headers
headers:
	echo ${subst image,headers,${KERNEL}} >> $@

# Automated preparation of the INSTALL CD package list
INSTALL_LIST_${ARCH}: SKIPLIST = server-skip
INSTALL_LIST_${ARCH}: ${INSTALLER} ${BY_PRIORITY} server
	${prepare_list}

# Automated preparation of the DESKTOP CD package list
DVD1_LIST_${ARCH}: SKIPLIST = desktop-skip
DVD1_LIST_${ARCH}: ${INSTALLER} ${BY_PRIORITY} server headers desktop by_vote-00
	${prepare_list}

# Automated preparation of the POOL1 ISO package list
POOL1_LIST_${ARCH}: SKIPLIST = /dev/null
POOL1_LIST_${ARCH}: LIMIT = 5000
POOL1_LIST_${ARCH}: pool1 firmware
	${prepare_list}

# Automated preparation of the POOL2 ISO package list
POOL2_LIST_${ARCH}: SKIPLIST = /dev/null
POOL2_LIST_${ARCH}: LIMIT = 15000
POOL2_LIST_${ARCH}: pool2 firmware
	${prepare_list}

# Automated preparation of the NETINSTALL CD package list
NETINSTALL_LIST_${ARCH}: SKIPLIST = /dev/null
NETINSTALL_LIST_${ARCH}: ${INSTALLER} all-required all-important
	${prepare_list}

# CD2 is a pool of packages in addition to INSTALL
CD2_LIST_${ARCH}: SKIPLIST = INSTALL_LIST_${ARCH}
CD2_LIST_${ARCH}: cd2-pool | INSTALL_LIST_${ARCH}
	${prepare_pool_list}

# CD3 is a pool of packages in addition to INSTALL
CD3_LIST_${ARCH}: SKIPLIST = INSTALL_LIST_${ARCH}
CD3_LIST_${ARCH}: cd3-pool | INSTALL_LIST_${ARCH}
	${prepare_pool_list}

# CD4 is a pool of packages in addition to INSTALL, CD2 and CD3
CD4_LIST_${ARCH}: SKIPLIST = INSTALL_LIST_${ARCH}
CD4_LIST_${ARCH}: SKIPLIST += CD2_LIST_${ARCH} CD3_LIST_${ARCH}
CD4_LIST_${ARCH}: cd4-pool | CD2_LIST_${ARCH} CD3_LIST_${ARCH}
	${prepare_pool_list}

# Prepare a local Packages file from the ${ISO} package list
${HEAD}_%_${TAIL}: library-${ARCH}-% ${ISO}_LIST_${ARCH}
	for f in $$(grep -v '#' ${ISO}_LIST_${ARCH}) ; do \
	    cat "$</$$f" 2>/dev/null || true ; \
	done > $@

clean:
	rm -f *_Packages *_LIST_${ARCH}

reallyclean: clean
	-rm -rf $(BY_PRIORITY) library-${ARCH}-*

.PHONY: by_vote_check
by_vote_check:

VOTE = https://popcon.debian.org/by_vote
by_vote-00 pool1 pool2: by_vote_check
	if [ ! -r $@ ] || [ $$(wc -l < $@) -ne ${LIMIT} ] ; then\
	    wget -O- --no-check-certificate ${VOTE} |\
	        grep -v '#' | awk 'NR <= ${LIMIT} {print $$2;}' > $@ ;\
	fi
	[ $$(wc -l < $@) -eq ${LIMIT} ]
