#!/usr/bin/env newlisp
#
# 
(signal 2 (fn (x) (exit 0)))

(setf
 VERSIONS (parse (replace "linux-image-" (main-args -1) "") "\\s+" 0)
 LIB (main-args -2)
 )

(write-line 2 (format "drop_wrong_kernel.lsp %s %s" LIB (string VERSIONS)))

(unless (ends-with LIB "debian-installer")
  (exit 0))

(dolist (P (exec (format "grep -H Kernel-Version: -r %s" LIB)))
  (when (regex "([^:]*):Kernel-Version: (.*)" P )
    (let ((F $1) (V $2))
      (unless (member V VERSIONS)
        (write-line 2 (format "dropping %s" F))
        (! (format "rm %s" F))
        ))))
  

(exit 0)
