#!/usr/bin/env newlisp
#
; This script attempts expand a package list given in one or more list
; files, based on a repository "library" of Packages files.
;
; : <library>+ : <package-list>+
;
; The library contains descriptions of available packages and their
; dependencies.

(signal 2 (fn (x) (exit 0)))

(if (match '(* ":" * ":" *) (main-args))
    (map set '(DUMMY PKGLISTS EXTRA) $it)
  (if (match '(* ":" * ) (main-args))
      (map set '(DUMMY PKGLISTS) $it)
    (begin (write-line 2 "Usage: : <package-list>+" )
           (exit 1))))

(setf ARCH ((exec "dpkg-architecture -qDEB_HOST_ARCH") 0))

; The LIBRARY table maps package name to the package declaration text
; block (without ending newlines).
(define LIBRARY:LIBRARY nil)

; The SIZE table maps package name to package size.
(define SIZE:SIZE nil)

; The IMPL table maps "provided" packages to the list of packages that
; provides it, in order of discovery.
(define IMPL:IMPL nil)

; The PROV table maps a package to all packages it provides.
(define PROV:PROV nil)

; The DEPS table maps a package to its dependency entries
(define DEPS:DEPS nil)

; The RECS table maps a package to its recommends entries
(define RECS:RECS nil)

; The USED table enumerates the collection of packages in use.
(define USED:USED nil)

; The INPUT table enumerates the input packages.
(define INPUT:INPUT nil)

(define USING:USING nil)
(define CHOICE:CHOICE nil)

(define (add-provision P) ; NAME provides P
  (if (IMPL P) (push NAME (IMPL P) (if (= P NAME) 0 -1)) (IMPL P (list NAME)))
  (if (PROV NAME) (push P (IMPL NAME) -1) (IMPL NAME (list P)))
  )

; Add dependency relations.
(define (add-depends D) ; NAME depends on comma separated Ds
  (setf D (replace "\\([^)]+\\)" D "" 0)) ; remove any versioning
  (setf D (map (fn (x) (trim (replace ":.*" x "" 0))) (parse D ",")))
  (if (DEPS NAME) (extend (DEPS NAME) D) (DEPS NAME D))
  )

; Add recommends relations
(define (add-recommends R) ; NAME recommends on comma separated Rs
  (setf R (replace "\\([^)]+\\)" R "" 0)) ; remove any versioning
  (setf R (map (fn (x) (trim (replace ":.*" x "" 0))) (parse R ",")))
  (RECS NAME (extend (or (RECS NAME) (list)) R))
  )

;; Process the library
(write-line 2 "Loading the Packages files")
(dolist
    (LIBDIR (filter directory? (directory "." (format "^library-%s-" ARCH))))
  (dolist (LIBFILE (directory LIBDIR "^[^.]"))
    (let ((BLOCK (or (read-file (format "%s/%s" LIBDIR LIBFILE)) ""))
          (NAME nil))
      (dolist (LINE (parse BLOCK "\n"))
        (when (and (regex "([^:]+): (.*)" LINE 0) (or NAME (= "Package" $1)))
          (case $1
            ("Package" (LIBRARY (setf NAME $2) BLOCK))
            ("Provides" (map add-provision (map trim (parse $2 ","))))
            ("Depends" (add-depends $2))
            ("Pre-Depends" (add-depends $2))
            ("Recommends" (add-recommends $2))
            ("Size" (SIZE NAME (int $2)))
            (true )) ; ignore all else
          ))
      (when NAME (add-provision NAME)) ; 
      )))

(write-line 2 "Loading the package lists")
(setf OPTIONS '())

(define (add-used P N)
  (when (< N (or (USED P) 100))
    (USED P N)
    (if (LIBRARY P)
        (begin
          (dolist (D (or (DEPS P) (list)))
            (let (R (string "DEP=" P))
              (if (find "|" D) (push (list D N) OPTIONS) (add-used D N))))
          (inc N)
          (dolist (R (or (RECS P) '()))
            (let (D (string "REC=" P))
              (if (find "|" R) (push (list R N) OPTIONS) (add-used R N))))
          )
      (IMPL P) (push (list (join (IMPL P) "|") N) OPTIONS)
      ; else it's a pure virtual
      )))

(dolist (PKGLIST PKGLISTS)
  (dolist (P (clean (fn (x) (or (empty? x) (find "#" x)))
                    (parse (or (read-file PKGLIST) "") "\n")))
    (INPUT P P)
    (add-used P 0)
    ))

(dolist (P (or EXTRA '())) (add-used P 0))

;(write-line 2 "Handling options")

(define NONE:NONE nil)

(define (some-impl-used P)
  (if (exists (fn (v) (USED v)) (or (IMPL P) '()))
      (USING (string "# using " $it " as " P " for " PL) true)))

(define (used-pkg P) (if (USED P) true nil))
(define (available-pkg P) (if (LIBRARY P) true nil))

(while (setf PN (pop OPTIONS))
  (let ((IT nil) (PL (map trim (parse (PN 0) "|"))))
    (cond ((setf IT (exists used-pkg PL))
           (USING (string "# using " IT " for " PL) true))
          ((setf IT (exists some-impl-used PL)) (add-used IT (PN 1)))
          ((setf IT (exists available-pkg PL)) (add-used IT (PN 1)))
          ((null? (NONE (PN 0)))
           (begin (NONE (PN 0) "TRUE") (push PN OPTIONS -1)))
          (true (CHOICE (PN 0) (string "# no choice for " PL)))
      )))

(map println (map first (USING)))
(map println (map first (CHOICE)))

(setf LVL -1 A 0 B 0 C 0)
(dolist (NP (sort (map reverse (USED))))
  (let (N (SIZE (NP 1)))
    (when N (if (INPUT (NP 1)) (inc A N) (= (NP 0)) (inc B N) (inc C N)))
    (when (!= LVL (NP 0)) (println "# Level " (setf LVL (NP 0))))
    (unless (INPUT (NP 1)) (println (NP 1)))))

(define (pretty-number n)
  (join (map reverse (reverse (explode (reverse (string n)) 3))) ","))

(define (pretty-numbers) (map pretty-number (args)))

(write-line 2 (println (format "# size %s + %s + %s = %s"
                               (pretty-numbers A B C (+ A B C)))))
(exit 0)
